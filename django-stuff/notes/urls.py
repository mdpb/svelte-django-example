from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('new/svelte/', views.svelte, name='new'),
    path('new/', views.new, name='new'),
    path('<int:note_id>/', views.view, name='view'),
    path('<int:note_id>/regular/', views.view, name='view'),
    path('svelte/', views.svelte, name='svelte'),
]