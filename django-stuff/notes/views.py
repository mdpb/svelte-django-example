from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.template import loader

from .models import Note

def index(request):
    latest_notes = Note.objects.order_by('created')[:15]
    template = loader.get_template('notes/index.html')
    context = {
        'latest_notes': latest_notes
    }
    return HttpResponse(template.render(context, request))

def new(request):
    return HttpResponse("basic django new")

def svelte(request):
    template = loader.get_template('notes/new-svelte.html')
    return HttpResponse(template.render(context=None, request=request))

def view(request, note_id):
    """Accepts a note object and returns a html template
    displaying the note.

    Args:
        request (django request): the context and request from the client
        note_id (int): Integer ID from a Note

    Returns:
        HttpResponse: view.html template
    """
    note = get_object_or_404(Note, pk=note_id)
    print(note)
    template = loader.get_template('notes/view.html')
    context = {
        'note': note
    }
    return HttpResponse(template.render(context, request))