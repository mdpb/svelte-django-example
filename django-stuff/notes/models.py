from django.db import models

class Note(models.Model):
    body = models.TextField()
    title = models.CharField(max_length=150)
    created = models.DateTimeField('date created')

    def __str__(self):
        return '%s %s %s' % (self.title, self.body, self.created)