some notes in `notes.md` to elaborate further.

step through git commits to see the changes that were needed. some experiments may live on branches.

# Goal
get a svelte page to load on django.  So django serves up this compiled `.js` and css when a user goes to http://127.0.0.1:8000/some-path.

#### django side:
1. launch webserver `$ poetry run python manage.py runserver`
1. user goes to localhost:8000/notes
1. `/notes` serves up a compiled svelte page
1. sample data and previous notes can be embedded + linked with special commands. API exists on django side to dump json
1. user can save a note

#### svelte side
1. compile with basic `node run build` via rollbar
1. write single page w/ subcomponents that has a UI to write text
1. N/A but next step: query the django server via an api (avoid cross origin requests). In theory the svelte page is already at the same localhost domain, so it can make api calls to itself?

I ended this without implementing the ability to POST/PUT data via a Django REST API. Ultimately I accomplished what I want but if you want a functional Svelte + Django setup you're gonna need something like djangorestframework & https://pypi.org/project/django-svelte/.