
# Goal
get a svelte page (no fancy spa stuff) to load on django.  So djago serves up this compiled `.js` and css when a user goes to http://127.0.0.1:8000/.

#### django side:
1. launch webserver `$ poetry run python manage.py runserver`
1. user goes to localhost:8000/notes
1. `/notes` serves up a compiled svelte page
1. sample data and previous notes can be embedded + linked with special commands. API exists on django side to dump json
1. user can save a note

#### svelte side
1. write single page w/ subcomponents that has a UI to write & edit text
1. use https://github.com/rob-balfre/svelte-select
1. query the django server via an api (avoid cross origin requests). In theory the svelte page is already at the same localhost domain, so it can make api calls to itself?

**Complications from svelte**
Post-ing data. 
Since we're no longer using Django's handy-dandy [forms](https://docs.djangoproject.com/en/3.1/intro/tutorial04/) we'll have to POST the data to a separate REST API on the django side :/.  

This is just a SO answer so I'm taking it with a grain of salt -> https://stackoverflow.com/questions/39972933/reactjs-django-forms but seems to confirm that there's no way to mix Django Forms w/ svelte.  So even if we use the nice [django-svelte plugin](https://github.com/thismatters/django-svelte/) it'll be a mess, seems like passing data w/ the django-svelte is a one way operation? i.e. you can pass it into a svelte app as a prop but not back up? At least not in without a REST API on the server side?

So assuming this is true:
a) ugly to mix svelte + django .html forms/templates
b) SPA components(react/vue/svelte) should make fetch requests to the API

Then:
SHOULD be able to toss the json result into an API.  Minimal work since there's no CORS? might be the easiest SPA + Server setup in the world... if it works I'll be a little bummed because I've sweated out CORS issues in the past.


### migrations in django
make a migration?
`python manage.py makemigrations notes`
`python manage.py sqlmigrate notes 0002`


-----fri may 5th edit-----
Outcomes so far:
I have copied the `npm run build` svelte output to the proper static directory for the django notes app.
I have confirmed the bundle.js loads, but the svelte app doesn't render. In fact the bundle.js file does nothing.